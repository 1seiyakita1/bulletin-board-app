class CreateComments < ActiveRecord::Migration[5.0]
  def change
    create_table :comments do |t|
      t.references :board, foreign_key: true # board_id :integer
      t.string     :name,    null: false     # name     :string(255)
      t.text       :comment, null: false     # comment  :text(65535)

      t.timestamps
    end
  end
end
